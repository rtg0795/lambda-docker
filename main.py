import json
import os
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    logger.info("New files uploaded to the source bucket.")
    logger.info("Event: {}".format(event))
    
    return {
        'statusCode': 200,
        'body': 'Lambda Invoked!'
    }
